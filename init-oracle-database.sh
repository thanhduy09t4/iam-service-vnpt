#!/usr/bin/env bash
mkdir -p choerodon_temp
if [ ! -f temp/choerodon-tool-liquibase.jar ]
then
    curl http://nexus.choerodon.com.cn/repository/choerodon-release/io/choerodon/choerodon-tool-liquibase/0.11.1.RELEASE/choerodon-tool-liquibase-0.11.1.RELEASE.jar -L  -o choerodon_temp/choerodon-tool-liquibase.jar
fi
java -Dspring.datasource.url="jdbc:oracle:thin:@10.193.79.127:1521/PDBIDG_DEV" \
 -Dspring.datasource.username=CIC_ERP \
 -Dspring.datasource.password=VnptERP@2020 \
 -Ddata.drop=false -Ddata.init=true \
 -Ddata.dir=src/main/resources \
 -jar choerodon_temp/choerodon-tool-liquibase.jar