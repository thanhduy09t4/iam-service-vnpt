const config = {
//  server: 'http://api.staging.saas.hand-china.com',
  // server: 'http://api.c7nf.choerodon.staging.saas.hand-china.com',
  server: 'http://localhost:8030',
  master: '@choerodon/master',
  projectType: 'choerodon',
  buildType: 'single',
  dashboard: {
    iam: {
      components: 'react/src/app/iam/dashboard/*',
      locale: 'react/src/app/iam/locale/dashboard/*',
    },
  },
  resourcesLevel: ['site', 'organization', 'project', 'user'],
};

// const config = {
//   master: './node_modules/@choerodon/master/lib/master.js',
//   modules: ['.'],
// };

module.exports = config;
